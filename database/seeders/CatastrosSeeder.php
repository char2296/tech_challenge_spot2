<?php

namespace Database\Seeders;

use App\Models\Catastro;
use Illuminate\Database\Seeder;

class CatastrosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->log("read file ");
        $csvFile = fopen(base_path("data/input_data_GAM.csv"), "r");

        $firstline = true;
        while (($data = fgetcsv($csvFile, 2000, ",")) !== FALSE) {
            if (!$firstline) {
                try {
                    Catastro::create([
                        "geo_shape" => json_encode($data[1]??[]),
                        "call_numero" => strval($data[2]),
                        "zip_code" => $data[3],
                        "colonia_predio" => $data[4],
                        "superficie_terreno" => $data[5],
                        "superficie_construccion" => $data[6],
                        "uso_construccion" => $data[7],
                        "clave_rango_nivel" => $data[8],
                        "anio_construccion" => $data[9],
                        "instalaciones_especiales" => $data[10],
                        "valor_unitario_suelo" => $data[11],
                        "valor_suelo" => $data[12],
                        "clave_valor_unitario_suelo" => $data[13],
                        "colonia_cumpliemiento" => $data[14],
                        "alcaldia_cumplimiento" => $data[15],
                        "subsidio" => $data[16]
                    ]);
                }catch (\Exception $e){
                    $this->log("DATA NOT VALID");
                }

            }
            $this->log("insert data ");
            $firstline = false;
        }
        fclose($csvFile);
        $this->log("close file ");
        $this->log("FINISH ");

    }

    public function log($string){
        print_r("LOG:: ".$string."\n");
    }

}
