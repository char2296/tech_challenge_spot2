<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCatastroTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catastros', function (Blueprint $table) {
            $table->id();
            $table->jsonb("geo_shape");
            $table->string("call_numero");
            $table->integer("zip_code");
            $table->string("colonia_predio");
            $table->string("superficie_terreno");
            $table->integer("superficie_construccion");
            $table->string("uso_construccion")->index();
            $table->integer("clave_rango_nivel");
            $table->integer("anio_construccion");
            $table->string("instalaciones_especiales");
            $table->float("valor_unitario_suelo")->index();
            $table->float("valor_suelo")->index();
            $table->string("clave_valor_unitario_suelo");
            $table->string("colonia_cumpliemiento");
            $table->string("alcaldia_cumplimiento");
            $table->float("subsidio")->index();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catastros');
    }
}
