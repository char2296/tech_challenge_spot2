<?php

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Requests\PriceRequest;
use App\Http\Resources\PriceResource;
use App\Services\PriceService;


class PriceController extends Controller
{


    private $service;
    public function __construct( PriceService $priceService){
        $this->service = $priceService;
    }

    public function index(PriceRequest $request, $zip_code, $operation){
        return new PriceResource($this->service->executeOperation(
            $operation,
            $zip_code,
            $request->input('construction_type'),
        ));
    }

}
