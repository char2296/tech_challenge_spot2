<?php

namespace App\Http\Requests;

use App\Enums\ConstructionTypeEnum;
use App\Enums\ValueComputedType;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PriceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "zip_code" => 'required|numeric',
            "operation" => ['required', Rule::in(ValueComputedType::TYPES)],
            "construction_type" => ['required',Rule::in(ConstructionTypeEnum::INDEX_TYPES)],
        ];
    }

    public function all($keys=null)
    {
        return array_replace_recursive(
            parent::all(),
            $this->route()->parameters()
        );
    }
}
