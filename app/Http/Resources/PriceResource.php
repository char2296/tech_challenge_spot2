<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PriceResource extends JsonResource
{
    public static $wrap = null;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "status" => true,
            "payload" => [
                "type"=> $this->type,
                "price_unit"=> $this->price_unit,
                "price_unit_construction"=>$this->price_unit_construction,
                "elements"=> $this->elements,
            ]
        ];
    }
}

