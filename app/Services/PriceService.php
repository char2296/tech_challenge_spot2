<?php

namespace App\Services;

use App\Enums\ConstructionTypeEnum;
use App\Enums\ValueComputedType;
use App\Repositories\PriceRepository;
use Illuminate\Support\Facades\DB;
use stdClass;

class PriceService
{
    private $repository;
    public function __construct( PriceRepository $priceRepository){
        $this->repository = $priceRepository;
    }

    public function executeOperation($operation, $zip_code,$type){
        $response = new stdClass();
        $construction_type_slug = ConstructionTypeEnum::SLUG_TYPES[$type];
        switch ($operation){
            case ValueComputedType::MAX:
                $data = $this->repository->getMaxPrice($zip_code,$construction_type_slug);
                break;
            case ValueComputedType::MIN:
                $data = $this->repository->getMinPrice($zip_code,$construction_type_slug);
                break;
            case ValueComputedType::AVG:
                $data = $this->repository->getAvgPrice($zip_code,$construction_type_slug);
                break;
        }
        $response->type = $operation;
        $response->price_unit = $data->price_unit;
        $response->price_unit_construction = $data->price_unit_construction;
        $response->elements = $data->total;
        return $response;
    }





}
