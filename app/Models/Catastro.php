<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Catastro extends Model
{
    use HasFactory;

    protected $fillable = [
        "geo_shape",
        "call_numero",
        "zip_code",
        "colonia_predio",
        "superficie_terreno",
        "superficie_construccion",
        "uso_construccion",
        "clave_rango_nivel",
        "anio_construccion",
        "instalaciones_especiales",
        "valor_unitario_suelo",
        "valor_suelo",
        "clave_valor_unitario_suelo",
        "colonia_cumpliemiento",
        "alcaldia_cumplimiento",
        "subsidio"
    ];

}
