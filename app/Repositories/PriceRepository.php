<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;

class PriceRepository
{


    public function getMaxPrice($zip_code,$type){
        return DB::table("catastros")
            ->selectRaw('MAX(superficie_terreno / (valor_suelo-subsidio)) as price_unit,
                                   MAX(superficie_construccion / (valor_suelo-subsidio)) as price_unit_construction,
                                   count(*) as total')
            ->where('zip_code',$zip_code)
            ->where('uso_construccion',$type)->first();
    }

    public function getMinPrice($zip_code,$type){
        return DB::table("catastros")
            ->selectRaw('MIN(superficie_terreno / (valor_suelo-subsidio)) as price_unit,
                                   MIN(superficie_construccion / (valor_suelo-subsidio)) as price_unit_construction,
                                   count(*) as total')
            ->where('zip_code',$zip_code)
            ->where('uso_construccion',$type)->first();
    }

    public function getAvgPrice($zip_code,$type){
        return DB::table("catastros")
            ->selectRaw('AVG(superficie_terreno / (valor_suelo-subsidio)) as price_unit,
                                   AVG(superficie_construccion / (valor_suelo-subsidio)) as price_unit_construction,
                                   count(*) as total')
            ->where('zip_code',$zip_code)
            ->where('uso_construccion',$type)->first();
    }





}
