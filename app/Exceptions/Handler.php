<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->renderable(function (MethodNotAllowedHttpException $exception, $request) {
            return response()->json(['status' => 'error', 'message' => $exception->getMessage()], 405);
        });

        $this->renderable(function (ValidationException $exception, $request) {
            if($request->expectsJson() || str_contains(request()->url(), '/api/')){
                return response()->json(['status' => 'error', 'message' => $exception->getMessage(), 'errors' => $exception->errors()], 422);
            }
        });



        $this->reportable(function (Throwable $e) {
            //
        });


    }
}
