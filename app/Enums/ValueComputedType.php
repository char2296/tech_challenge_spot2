<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class ValueComputedType extends Enum
{
    const MIN = 'min';
    const MAX = 'max';
    const AVG = 'avg';

    const TYPES = [self::MIN,self::MAX,self::AVG];



}
