<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class ConstructionTypeEnum extends Enum
{
    const INDEX_GREEN_AREAS = 1;
    const INDEX_NEIGHBORHOOD_CENTER = 2;
    const INDEX_EQUIPMENT = 3;
    const INDEX_HOUSING = 4;
    const INDEX_RESIDENTIAL_AND_COMMERCIAL = 5;
    const INDEX_INDUSTRIAL = 6;
    const INDEX_NO_ZONING = 7;

    const SLUG_GREEN_AREAS = 'Áreas verdes';
    const SLUG_NEIGHBORHOOD_CENTER = 'Centro de barrio';
    const SLUG_EQUIPMENT = 'Equipamiento';
    const SLUG_HOUSING = 'Habitacional';
    const SLUG_RESIDENTIAL_AND_COMMERCIAL = 'Habitacional y comercial';
    const SLUG_INDUSTRIAL = 'Industrial';
    const SLUG_NO_ZONING = 'Sin Zonificación';

    const INDEX_TYPES = [
       self::INDEX_GREEN_AREAS,
       self::INDEX_NEIGHBORHOOD_CENTER,
       self::INDEX_EQUIPMENT,
       self::INDEX_HOUSING,
       self::INDEX_RESIDENTIAL_AND_COMMERCIAL,
       self::INDEX_INDUSTRIAL,
       self::INDEX_NO_ZONING,
    ];
    const SLUG_TYPES = [
        self::INDEX_GREEN_AREAS => self::SLUG_GREEN_AREAS,
        self::INDEX_NEIGHBORHOOD_CENTER=>self::SLUG_NEIGHBORHOOD_CENTER,
        self::INDEX_EQUIPMENT=>self::SLUG_EQUIPMENT,
        self::INDEX_HOUSING=>self::SLUG_HOUSING,
        self::INDEX_RESIDENTIAL_AND_COMMERCIAL=>self::SLUG_RESIDENTIAL_AND_COMMERCIAL,
        self::INDEX_INDUSTRIAL=>self::SLUG_INDUSTRIAL,
        self::INDEX_NO_ZONING=>self::SLUG_NO_ZONING,
    ];



}
