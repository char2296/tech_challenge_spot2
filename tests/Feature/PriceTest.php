<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PriceTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */

    public function testSuccess()
    {

        $this->json('GET', '/price-m2/zip-codes/7801/aggregate/max?construction_type=2', ['Accept' => 'application/json'])
            ->assertStatus(200)->assertJsonStructure([
                "status" ,
                "payload" => [
                    "type",
                    "price_unit",
                    "price_unit_construction",
                    "elements",
                ]
            ]);
    }


    public function testFailed()
    {
        $this->json('GET', '/price-m2/zip-codes/7801/aggregate/otra', ['Accept' => 'application/json'])
            ->assertStatus(200)->assertJsonStructure([
                "status",
	            "message",
	            "errors"
            ]);
    }
}
