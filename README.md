# Technical Challenge Spot 

##### URL BASE: 
    http://137.184.107.227/api

#### GET PRICE:
    GET /price-m2/zip-codes/{zip_code}/aggregate/{operation}?construction_type={1-7}

##### URL PARAMS:
    Name: zip_code
    Description: Codigo postal a consultar
    Validation: number , required 

---
    Name: operation
    Description: Llave de la operación a consultar
    Validation: string , required
    Valid Values: min | max  | avg
---
    Name: construction_type
    Description: Llave del tipo de contruacción
    Validation: string , required
    Valid Values:
        1) Áreas verdes
        2) Centro de barrio
        3) Equipamiento
        4) Habitacional
        5) Habitacional y comercial
        6) Industrial
        7) Sin Zonificación
---
##### RESPONSE
###### SUCCESS RESPONSE 
`
{
"status": true,
"payload": {
"type": "avg",
"price_unit": 1420,
"price_unit_construction": 3120,
"elements": 100
} }` 

###### ERROR RESPONSE 
`{
"status": "error",
"message": "The given data was invalid.",
"errors": {
"construction_type": [
"The construction type field is required."
]
}
}` 
